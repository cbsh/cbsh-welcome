# CBSH Welcome Application

## About the App
CBSH Welcome is a  welcomer (and Tweaker) to help people new to using my post install script learn a bit more about it and set some basic things such as the default themes (included with the qtile config) and your user shell.

### The Main Window
![](https://gitlab.com/cbsh/cbsh-welcome/-/raw/main/images/Main-Window.png)

### The Shell Changer
![](https://gitlab.com/cbsh/cbsh-welcome/-/raw/main/images/Shell-Changer.png)

### The Colour Scheme Changer (referred to as themes throughout CBSH)
![](https://gitlab.com/cbsh/cbsh-welcome/-/raw/main/images/Colour-Scheme-Changer.png)

## Installing the Application (on CBSH)
Via CBSH, it should be installed already, but if for whatever reason it is not, you can install it from the CBSH repo with:
`sudo pacman -S cbsh-welcome`

## Installing the Application (on Arch based Systems not running CBSH)
Grab the PKGBUILD from the repo, and then run the following command:
`makepkg -cf`

This will create a file that ends in .pkg.tar.zst (e.g. cbsh-welcome-\*.\*.\*-x86_64.pkg.tar.zst). Then Run:
`sudo pacman -U *.pkg.tar.zst`

OR

Add my CBSH repo to pacman

```bash
[cbsh-arch-repo]
SigLevel = Required DatabaseOptional
Server https://gitlab.com/cbsh/cbsh-arch/$repo/-/raw/main/$arch
```
and sync your repos with `sudo pacman -Syyu`, you can then use pacman to install: `sudo pacman -S cbsh-welcome`

