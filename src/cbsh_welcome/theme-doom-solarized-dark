#!/usr/bin/env bash
cd $HOME/.config/qtile
theme='cbsh-doom-solarized-dark'
ln -sf themes/$theme/$theme.py currentTheme.py
rm -r __pycache__
qtile cmd-obj -o cmd -f restart

# Tell emacsclient to load a theme for the current session,
# and update literate config with new value so its persistent with new emacs windows
emacsclient --eval "(load-theme '$theme)"
emacsclient --eval "(dashboard-refresh-buffer)"

sed -i "113c(setq doom-theme \'$theme)" $HOME/.config/doom/config.org
emacsclient --eval "(doom/reload)"

# Recompile ST with new theme.
# We do this as Suckless tools need to be recompiled everytime we make a change.
cd /opt/st-cbsh
sudo -A sed -i "108c#include \"themes/$theme.h\"" config.h
sudo make clean install

# Recompile dmenu with new theme
cd /opt/dmenu-cbsh
sudo -A sed -i "23c#include \"themes/$theme.h\"" config.h
sudo make clean install

# Change Alacritty Theme
cd $HOME/.config/alacritty
sed -i "306ccolors: *$theme" alacritty.yml

# Update SDDM Theme
sudo sed -i 's/^background=*.*/background=$theme.png/g'

# Update Lockscreen
betterlockscreen -u "$HOME/.config/qtile/themes/$theme/$theme.png"
exit 0
