#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib, GdkPixbuf

import threading
import time
import tempfile
import os
from os.path import exists, expanduser
from pathlib import Path
from wand.image import Image

import shutil
import subprocess

global home
global user
global source
global dest
global settings
global bash_exists
global zsh_exists
global fish_exists
global defaultTextBuffer
global gymLeaderTextBuffer

home = expanduser("~")
user = os.getlogin()
source = "/usr/share/applications/cbsh-welcome.desktop"
dest = home + "/.config/autostart/cbsh-welcome.desktop"
settings = home + "/.config/cbsh-welcome/"
pacman = "/etc/pacman.conf"
mirrorlist = "/etc/pacman.d/mirrorlist"
arandrCheck = subprocess.run(["pacman", "-Qi", "arandr"])
alacrittyCheck = subprocess.run(["pacman", "-Qi", "alacritty"])
kittyCheck = subprocess.run(["pacman", "-Qi", "kitty"])
urxvtCheck = subprocess.run(["pacman", "-Qi", "rxvt-unicode"])

builder = Gtk.Builder()
builder.add_from_file("cbsh-welcome.glade")

bash_exists = '/bin/bash'
zsh_exists = '/bin/zsh'
fish_exists = '/bin/fish'

if Path(bash_exists).is_file():
    print(f'Bash is Installed')
else:
    print(f'Bash is not Installed')

if Path(zsh_exists).is_file():
    print(f'Zsh is Installed')
else:
    print(f'Zsh is not Installed')

if Path(fish_exists).is_file():
    print(f'Fish is Installed')
else:
    print(f'Fish is not Installed')

# Setup Magical Threading Function that does fun stuff and that
def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()
    return wrapper

noChangeShellRadio = builder.get_object("noChangeShellRadio")
fishShellRadio = builder.get_object("fishShellRadio")
zshShellRadio = builder.get_object("zshShellRadio")
bashShellRadio = builder.get_object("bashShellRadio")
changeShellLabel = builder.get_object("changeShellLabel")


learnMoreDialog = builder.get_object("learnMoreDialog")
learnMoreBtn = builder.get_object("learnMoreBtn")
learnMoreExitBtn = builder.get_object("learnMoreExitBtn")

### APPEARANCE SECTION ###
noChangeThemeRadio = builder.get_object("noChangeThemeRadio")
cbshVibrantRadio = builder.get_object("cbshVibrantRadio")
cbshBlackHoleRadio = builder.get_object("cbshBlackHoleRadio")
doomOneRadio = builder.get_object("doomOneRadio")
doomDraculaRadio = builder.get_object("doomDraculaRadio")
doomSolarizedDarkRadio = builder.get_object("doomSolarizedDarkRadio")
changeThemeLabel = builder.get_object("changeThemeLabel")
defaultThemesTerminalOutput = builder.get_object("defaultThemesTerminalOutput")

noChangeGymLeaderThemeRadio = builder.get_object("noChangeGymLeaderThemeRadio")
dianthaRadio = builder.get_object("dianthaRadio")
elesaRadio = builder.get_object("elesaRadio")
elesaAltRadio = builder.get_object("elesaAltRadio")
shauntalRadio = builder.get_object("shauntalRadio")
korrinaRadio = builder.get_object("korrinaRadio")
roxieRadio = builder.get_object("roxieRadio")
skylaRadio = builder.get_object("skylaRadio")
valerieRadio = builder.get_object("valerieRadio")
cynthiaRadio = builder.get_object("cynthiaRadio")
flanneryRadio = builder.get_object("flanneryRadio")
sidneyradio = builder.get_object("sidneyradio")
changeGymLeaderThemeLabel = builder.get_object("changeGymLeaderThemeLabel")
gymLeaderThemeOutput = builder.get_object("gymLeaderThemeOutput")

launchArandrBtn = builder.get_object("launchArandrBtn")
updateXrandrEntry = builder.get_object("updateXrandrEntry")
updateXrandrBtn = builder.get_object("updateXrandrBtn")
arandrInstalledLabel = builder.get_object("arandrInstalledLabel")

grubFileChooserBtn = builder.get_object("grubFileChooserBtn")
grubFileChooserDialog = builder.get_object("grubFileChooserDialog")
closeFileChooserBtn = builder.get_object("closeFileChooserBtn")
grubTimeoutScale = builder.get_object("grubTimeoutScale")
grubBackgroundImagesView = builder.get_object("grubBackgroundImagesView")
grubApplyTimeoutBtn = builder.get_object("grubApplyTimeoutBtn")

alacrittyInstallBtn = builder.get_object("alacrittyInstallBtn")
kittyInstallBtn = builder.get_object("kittyInstallBtn")
urxvtInstallBtn = builder.get_object("urxvtInstallBtn")

archTestingRepoSwitch = builder.get_object("archTestingRepoSwitch")

# ARandR Check, will update a label in app to let the user know if it is installed or not.
if arandrCheck.returncode == 0:
    arandrInstalledLabel.set_text("ARandR Installed")
    launchArandrBtn.set_sensitive(True)
else:
    arandrInstalledLabel.set_text("ARandR Not Installed, please install to use ARandR")
    launchArandrBtn.set_sensitive(False)

# Terminal Checks, will check to see if they are installed and disable/enable the install button
if alacrittyCheck.returncode == 0:
   alacrittyInstallBtn.set_sensitive(False)
   # Sensitivity is the GTK equivalent of enabling and
   # disabling interation in other toolkits,
   # so to disable a button, you set its sensitivity to False, and vice versa
   alacrittyInstallBtn.set_label("Alacritty is Installed")
else:
    alacrittyInstallBtn.set_sensitive(True)
    alacrittyInstallBtn.set_label("Install")

if kittyCheck.returncode == 0:
    kittyInstallBtn.set_sensitive(False)
    kittyInstallBtn.set_label("Kitty is Installed")
else:
    kittyInstallBtn.set_sensitive(True)
    kittyInstallBtn.set_label("Install")

if urxvtCheck.returncode == 0:
    urxvtInstallBtn.set_sensitive(False)
    urxvtInstallBtn.set_label("urxvt is Installed")
else:
    urxvtInstallBtn.set_sensitive(True)
    urxvtInstallBtn.set_label("Install")

# Pacman Checks, will check to see if the CBSH Repos are installed (for standalone installs of this program) and check to see what stock repos are enabled
def check_repo(value):
        with open(fn.pacman, "r", encoding="utf-8") as myfile:
            lines = myfile.readlines()
            myfile.close()

        for line in lines:
            if value in line:
                if "#" + value in line:
                    return False
                else:
                    return True
            return False

# Grub Background Data
fuHua = Image

@threaded
class defaultThemeChangeLogic():
    def __init__(self, theme):
        self.theme = theme
        po = subprocess.run(self.theme, shell = True, capture_output=True, text=True)
        po_out = po.stdout
        GLib.io_add_watch(po_out, GLib.IO_IN, self.writeToBuffer)

    def writeToBuffer(self, fd, condition):
        if condition == GLib.IO_IN:
            char = fd.read(1)
            defaultTextBuffer = defaultThemesTerminalOutput.get_buffer()
            defaultTextBuffer.insert_at_cursor(char)
            return True
        else:
            return False


@threaded
class gymLeaderThemeChangeLogic():
    def __init__(self, theme):
        self.theme = theme
        po = subprocess.run(self.theme, shell = True, capture_output=True, text=True)
        po_out = po.stdout
        GLib.io_add_watch(po_out, GLib.IO_IN, self.writeToBuffer)

    def writeToBuffer(self, fd, condition):
        if condition == GLib.IO_IN:
            char = fd.read(1)
            gymLeaderTextBuffer = gymLeaderThemeOutput.get_buffer()
            gymLeaderTextBuffer.insert_at_cursor(char)
            return True
        else:
            return False

class Handler:
    def on_MainExitBtn_clicked(self, widget):
        Gtk.main_quit()


    # TERMINAL SHELL TAB
    def on_changeShellBtn_clicked(self, widget):
        if noChangeShellRadio.get_active():
            changeShellLabel.set_text("Shell not changed")

        if bashShellRadio.get_active():
            print ("Setting bash as", user, "'s shell")
            if Path(bash_exists).is_file():
                print(f'/bin/bash exists.')
                changeShellLabel.set_text("Shell changed to bash")
                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/bash"])
            else:
                changeShellLabel.set_text(shell, "is not installed.")

        if zshShellRadio.get_active():
            print ("Setting zsh as", user, "'s shell")
            if Path(zsh_exists).is_file():
                print(f'/bin/zsh exists.')
                changeShellLabel.set_text("Shell changed to zsh")
                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/zsh"])
            else:
                print(shell, "is not installed.")

        if fishShellRadio.get_active():
            print ("Setting fish as", user, "'s shell")
            if Path(fish_exists).is_file():
                print(f'/bin/fish exists.')
                changeShellLabel.set_text("Shell changed to fish")
                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/fish"])
            else:
                print(shell, "is not installed.")

    # DEFAULT THEME TAB
    def on_changeThemeBtn_clicked(self, widget):
        if noChangeThemeRadio.get_active():
            changeThemeLabel.set_text("Theme not changed")

        if cbshVibrantRadio.get_active():
            changeThemeLabel.set_text("Theme changed to CBSH Vibrant")
            defaultThemeChangeLogic("./theme-cbsh-vibrant")

        if cbshBlackHoleRadio.get_active():
            changeThemeLabel.set_text("Theme changed to CBSH Black Hole")
            defaultThemeChangeLogic("./theme-cbsh-black-hole")

        if doomOneRadio.get_active():
            changeThemeLabel.set_text("Theme changed to Doom One")
            defaultThemeChangeLogic("./theme-doom-one")


        if doomDraculaRadio.get_active():
            changeThemeLabel.set_text("Theme changed to Doom Dracula")
            defaultThemeChangeLogic("./theme-doom-dracula")

        if doomSolarizedDarkRadio.get_active():
            changeThemeLabel.set_text("Theme changed to Doom Solarized Dark")
            defaultThemeChangeLogic("./theme-doom-solarized-dark")


    # POKEMON GYM LEADER THEME TAB
    def on_changeGymLeaderThemeBtn_clicked(self, widget):
        if noChangeGymLeaderThemeRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme not changed")

        if dianthaRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Diantha")
            gymLeaderThemeChangeLogic("./theme-diantha")

        if elesaRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Elesa")
            gymLeaderThemeChangeLogic("./theme-elesa")

        if elesaAltRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Elesa (B2/W2 Outfit)")
            gymLeaderThemeChangeLogic("./theme-elesa-alt")

        if shauntalRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Shauntal")
            gymLeaderThemeChangeLogic("./theme-shauntal")

        if korrinaRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Korrina")
            gymLeaderThemeChangeLogic("./theme-korrina")

        if roxieRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Roxie")
            gymLeaderThemeChangeLogic("./theme-roxie")

        if skylaRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Skyla")
            gymLeaderThemeChangeLogic("./theme-skyla")

        if valerieRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Valerie")
            gymLeaderThemeChangeLogic("./theme-valerie")

        if cynthiaRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Cynthia")
            gymLeaderThemeChangeLogic("./theme-cynthia")

        if flanneryRadio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Flannery")
            gymLeaderThemeChangeLogic("./theme-flannery")

        if sidneyradio.get_active():
            changeGymLeaderThemeLabel.set_text("Theme changed to Sidney")
            gymLeaderThemeChangeLogic("./theme-sidney")

    # Learn More Dialog
    def on_learnMoreBtn_clicked(self, widget):
        learnMoreDialog.show()

    def on_learnMoreExitBtn_clicked(self, widget):
        learnMoreDialog.hide()

    #RESOLUTION TAB
    def on_launchArandrBtn_clicked(self, widget):
        subprocess.run(["arandr"])

    def on_updateXrandrBtn_clicked(self, widget):
        options = updateXrandrEntry.get_text()
        splitoptions = options.split()
        subprocess.run(splitoptions)
        subprocess.run(["sed", "-i", f"4c{options}" , home + "/.config/qtile/autostart.sh"])

    # TERMINALS STACK
    def on_alacrittyInstallBtn_clicked(self,widget):
        subprocess.run(["sudo", "-A" ,"pacman", "-S", "--noconfirm", "alacritty"])
    def on_kittyInstallBtn_clicked(self,widget):
        subprocess.run(["sudo", "-A" ,"pacman", "-S", "--noconfirm", "kitty"])
    def on_urxvtInstallBtn_clicked(self,widget):
        subprocess.run(["sudo", "-A", "pacman", "-S", "--noconfirm", "rxvt-unicode"])


    # GRUB THEMES STACK

#    def on_grubFileChooserBtn_clicked(self, widget):
#        response = grubFileChooserDialog.run()
#        print(response)
#        if response == Gtk.ResponseType.OK:
#            filename = grubFileChooserDialog.get_filename()
#          print(filename)
#            # grubFileChooserDialog.destroy()
#        elif response == Gtk.ResponseType.CANCEL:
#            print(Closing)

builder.connect_signals(Handler())

window = builder.get_object("MainWindow")
window.show_all()
window.connect("destroy", Gtk.main_quit)

Gtk.main()
